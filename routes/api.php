<?php

Route::post('/product', 'ProductController@createProduct');

Route::post('/order', 'OrderController@createOrder');

Route::get('/orders/{product_type?}', 'OrderController@listOrders');
