Project created on **PHP 7.2.14**, **MySQL 5.7.24**.

# Installation

1. create database
2. copy .env.example to .env and specify database credentials in .env
3. composer install
4. run migrations: php artisan migrate  

# Testing

Note: for POST requests bodies should be posted as
Content-Type: application/json

### Create product
POST http://print.local/api/product

    {
        "price": 25,
        "type": "type_a",
        "color_code": "020506",
        "size": 52
    }

### Create order
POST http://print.local/api/order

    {
        "order": [
            { "product_id": 4, "amount": 2 }
        ]
    }


### List orders
GET http://print.local/api/orders

### List orders by product type
GET http://print.local/api/orders/type_b