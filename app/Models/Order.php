<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Order
 * @package App\Models
 *
 * @property Collection $items
 *
 * @method static self create(array $attributes = [])
 */
class Order extends Model
{
    public $fillable = ['country_code'];
    protected $appends = ['price'];

    public function items() : HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    public function getPrice() : float
    {
        $price = $this->items->sum(function (OrderItem $item) {
            return $item->getPrice();
        });

        return round($price, 2);
    }

    public function getPriceAttribute()
    {
        return $this->getPrice();
    }
}
