<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * Class Product
 * @package App\Models
 *
 * @property int $id
 * @property float $price
 *
 * @method static Builder whereIn($column, $values, $boolean = 'and', $not = false)
 * @method static self create(array $attributes = [])
 */
class Product extends Model
{
    public $fillable = ['price', 'type', 'color_code', 'size'];
}
