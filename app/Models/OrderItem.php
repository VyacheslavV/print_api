<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class OrderItem
 * @package App\Models
 *
 * @property float $amount
 *
 * @property Product $product
 */
class OrderItem extends Model
{
    public $timestamps = false;
    public $fillable = ['product_id', 'amount'];

    public function product() : HasOne
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function getPrice() : float
    {
        $price = 0;

        if ($this->product) {
            $price = $this->product->price * $this->amount;
        }

        return $price;
    }
}
