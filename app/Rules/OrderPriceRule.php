<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;

class OrderPriceRule implements Rule
{

    public function passes($attribute, $order) : bool
    {
        $passes = true; // if there are no products - allow other rules to act

        if (is_array($order)) {
            $idToAmount = [];

            foreach ($order as $item) {
                if (array_key_exists('product_id', $item)) {
                    $productId = intval($item['product_id']);
                    $amount = array_key_exists('amount', $item) ? intval($item['amount']) : 1;
 
                    if (!array_key_exists($productId, $idToAmount)) {
                        $idToAmount[$productId] = $amount;
                    } else {
                        $idToAmount[$productId] += $amount;
                    }
                }
            }

            if (count($idToAmount)) {
                /** @var Collection $productCollection */
                $productCollection = Product::whereIn('id', array_keys($idToAmount))->get();

                $orderPrice = $productCollection
                    ->sum(function (Product $product) use ($idToAmount) {
                        return $product->price * $idToAmount[$product->id];
                    });

                $passes = $orderPrice >= floatval(config('print_api.min_order_price'));
            }
        }

        return $passes;
    }

    public function message()
    {
        return 'Order price is less then ' . config('print_api.min_order_price');
    }
}
