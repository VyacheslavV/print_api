<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Models\Order;
use Illuminate\Database\Eloquent\Builder;

class OrderController extends Controller
{
    public function createOrder(OrderRequest $request)
    {
        $order = Order::create($request->post());
        $order->items()->createMany($request->post('order'));

        return response()->json([
            'price' => $order->getPrice(),
        ]);
    }

    public function listOrders(string $productType = '')
    {
        $orderQuery = Order::with(['items', 'items.product']);

        if (strlen($productType)) {
            $orderQuery->whereHas('items.product', function(Builder $query) use ($productType) {
                $query->where('type', $productType);
            });
        }

        $orderCollection = $orderQuery->get();

        return response()->json([
            'orders' => $orderCollection,
        ]);
    }
}
