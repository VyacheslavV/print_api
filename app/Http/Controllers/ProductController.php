<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;

class ProductController extends Controller
{
    public function createProduct(ProductRequest $request)
    {
        $product = Product::create($request->post());

        return response()->json([
            'product_id' => $product->id,
        ]);
    }
}
