<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use \Torann\GeoIP\Location;

class AddCountryCode
{
    private const DEFAULT_COUNTRY_CODE = 'US';

    public function handle(Request $request, \Closure $next)
    {
        $this->setCountryCode($request);

        return $next($request);
    }

    private function setCountryCode(Request $request)
    {
        /** @var Location */
        $location = geoip($request->ip());
        $countryCode = self::DEFAULT_COUNTRY_CODE;

        if ($location instanceof Location) {
            $countryCode = $location->iso_code;
        }

        $request->request->add(['country_code' => $countryCode]);
    }
}
