<?php

namespace App\Http\Requests;

use App\Rules\OrderPriceRule;


class OrderRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'order' => [
                new OrderPriceRule(),
            ],
            'order.*' => 'required|array|min:1',
            'order.*.product_id' => 'required|exists:products,id',
            'order.*.amount' => 'required|numeric|min:1|max:255',
        ];
    }
}
