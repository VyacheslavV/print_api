<?php

namespace App\Http\Requests;

use \Illuminate\Database\Query\Builder;
use Illuminate\Validation\Rule;

class ProductRequest extends BaseRequest
{
    public function rules()
    {
        $request = $this;

        return [
            'price' => 'required|numeric|min:0.01|max:999999.99',
            'type' => 'required|string|max:100',
            'color_code' => 'required|string|regex:/^[A-Fa-f0-9]{6}$/',
            'size' => [
                'required',
                'numeric',
                'min:0.01',
                'max:999999.99',
                Rule::unique('products')->where(function (Builder $query) use ($request) {
                    return $query
                        ->where('type', $request->post('type'))
                        ->where('color_code', $request->post('color_code'))
                        ->where('size', $request->post('size'));
                }),
            ],
        ];
    }

    public function messages()
    {
        return [
          'size.unique' => 'This type, color_code and size already exists',
        ];
    }
}
